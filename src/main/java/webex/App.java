package webex;

import java.net.*;
import java.io.*;

public class App {
    public static void main(String[] args) throws Exception {
        String siteName = "apidemoeu";                  // WebEx site name
        String xmlURL = "WBXService/XMLService";    // XML API URL
        String siteID = "690319";                     // Site ID
        String partnerID = "g0webx!";                  // Partner ID
        String webExID = "nishitai";                // Host username
        String password = "gitpush0A";               // Host password

        String xmlServerURL = "https://" + siteName + ".webex.com/" + xmlURL;

        // connect to XML server
        URL urlXMLServer = new URL(xmlServerURL);

        // URLConnection supports HTTPS protocol only with JDK 1.4+
        URLConnection urlConnectionXMLServer = urlXMLServer.openConnection();
        urlConnectionXMLServer.setRequestProperty("Content-Type", "application/xml; charset=utf-8");
        urlConnectionXMLServer.setDoOutput(true);

        // send request
        PrintWriter out = new PrintWriter(urlConnectionXMLServer.getOutputStream());
        String reqXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
        reqXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
        reqXML += " xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\"";
        reqXML += " xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service\">\r\n";
        reqXML += "<header>\r\n";
        reqXML += "<securityContext>\r\n";
        reqXML += "<webExID>" + webExID + "</webExID>\r\n";
        reqXML += "<password>" + password + "</password>\r\n";
        reqXML += "<siteID>" + siteID + "</siteID>\r\n";
        reqXML += "<partnerID>" + partnerID + "</partnerID>\r\n";
        reqXML += "</securityContext>\r\n";
        reqXML += "</header>\r\n";
        reqXML += "<body>\r\n";
        reqXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.LstsummaryMeeting\">";
        reqXML += "<listControl>";
        reqXML += "<maximumNum>5</maximumNum>";
        reqXML += "</listControl>";
        reqXML += "<order>";
        reqXML += "<orderBy>STARTTIME</orderBy>";
        reqXML += "</order>";
        reqXML += "</bodyContent>\r\n";
        reqXML += "</body>\r\n";
        reqXML += "</serv:message>\r\n";
        out.println(reqXML);
        out.close();
        System.out.println("XML Request POSTed to " + xmlServerURL + "\n");
        System.out.println(reqXML + "\n");

        // read response
        BufferedReader in = new BufferedReader(new
                InputStreamReader(urlConnectionXMLServer.getInputStream()));
        String line;
        String respXML = "";
        while ((line = in.readLine()) != null) {
            respXML += line;
        }
        in.close();

        // output response
        respXML = URLDecoder.decode(respXML, "UTF-8");
        System.out.println("\nXML Response\n");
        System.out.println(respXML);
    }
}