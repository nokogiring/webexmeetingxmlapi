package webex;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class XMLReader {

    public static String readAll(String path) throws IOException {
        return Files.lines(Paths.get(path))
                .reduce("", (prev, line) ->
                        prev + line + System.getProperty("line.separator"));
    }

}
