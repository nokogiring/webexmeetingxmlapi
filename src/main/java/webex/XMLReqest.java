package webex;

import java.net.*;
import java.io.*;


public class XMLReqest {

    public static void requestAndShow(String path) throws IOException {
        String siteName = "apidemoeu";                  // WebEx site name
        String xmlURL = "WBXService/XMLService";    // XML API URL

        String xmlServerURL = "https://" + siteName + ".webex.com/" + xmlURL;

        // connect to XML server
        URL urlXMLServer = new URL(xmlServerURL);

        URLConnection urlConnectionXMLServer = urlXMLServer.openConnection();
        urlConnectionXMLServer.setRequestProperty("Content-Type", "application/xml; charset=utf-8");
        urlConnectionXMLServer.setDoOutput(true);

        PrintWriter out = new PrintWriter(urlConnectionXMLServer.getOutputStream());

        String reqXML = XMLReader.readAll(path);

        System.out.println(reqXML);

        out.println(reqXML);
        out.close();
        System.out.println("XML Request POSTed to " + xmlServerURL + "\n");
        System.out.println(reqXML + "\n");

        BufferedReader in = new BufferedReader(new
                InputStreamReader(urlConnectionXMLServer.getInputStream()));
        String line;
        String respXML = "";
        while ((line = in.readLine()) != null)

        {
            respXML += line;
        }
        in.close();

        respXML = URLDecoder.decode(respXML, "UTF-8");
        System.out.println("\nXML Response\n");
        System.out.println(respXML);

    }

}
